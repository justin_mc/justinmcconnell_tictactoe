threeInARowConditions = [7, 56, 448, 73, 146, 292, 273, 84];
var x_score = 0;
var o_score = 0;

//True if it's x's turn, false if it's o's turn.
var x_turn = true;
var current_image="images/x_image.png";

//After each turn, checks to see if somebody wins.
function checkForWin() {
	
	//Loop through the conditions. If any of the 8 conditions
	//match the "three in a row" score, represented by
	//each successive box ^2, return true. Otherwise, false.
	for (i = 0; i < threeInARowConditions.length; i++) {
		
		if ((threeInARowConditions[i] & x_score) === (threeInARowConditions[i])) {
			document.getElementById("message").innerHTML ="x wins!";
			return true;
		}
		
		if ((threeInARowConditions[i] & o_score) === (threeInARowConditions[i])) {
			document.getElementById("message").innerHTML ="o wins!";
			return true;
		}
	}
	return false;
}

//Switch out the pictures and increment the score.
function tileSelected(image, tile) {
		image.src= current_image;
		if(x_turn) {
			x_score = x_score + tile;
		} else {
			o_score = o_score + tile;
		}
		switchTurns();
}

//Change the current turn.
function switchTurns() {
	if(x_turn) {
		current_image = "images/o_image.png"
		document.getElementById("message").innerHTML ="o's turn!";
		x_turn = false;
	} else {
		current_image = "images/x_image.png"
		document.getElementById("message").innerHTML ="x's turn!";
		x_turn = true;
	}
	checkForWin();
}

